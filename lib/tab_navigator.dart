import 'package:flutter/material.dart';
import 'content_pager.dart';

/**
 * 创建人：xuqing
 * 创建时间：2020年2月7日16:26:18
 *
 */


class TabNavigator extends StatefulWidget {
  TabNavigator({Key key}) : super(key: key);

  @override
  _TabNavigatorState createState() {
    return _TabNavigatorState();
  }
}

class _TabNavigatorState extends State<TabNavigator> {

  final _defaultColor=Colors.grey;  //未选中
  final _activeColor=Colors.blue;  //选中的颜色
  int  _currentIndex=0;
  final ContentPagerConteroller _contentPagerConteroller=new ContentPagerConteroller();

  @override
  void initState() {
    super.initState();

}
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
             Color(0xffedeef0),
             Color(0xffe6e7E9),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter
          )
        ),
        child: ContentPager(
          contentPagerConteroller: _contentPagerConteroller,
          onPageChanged: (int index){
            setState(() {
              _currentIndex=index;
            });
          },
        )

      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          _bottomItem("本周", Icons.folder, 0),
          _bottomItem("分享", Icons.explore, 1),
          _bottomItem("免费", Icons.donut_small, 2),
          _bottomItem("长按", Icons.person, 3),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        onTap: (index){
          _contentPagerConteroller.jumpTopage(index);
          setState(() {
            _currentIndex=index;
          });
        },
      ),
    );

  }
  //封装底部的tab
   _bottomItem(String title, IconData icon ,int index){
      return BottomNavigationBarItem(
        icon: Icon(
          icon,
          color: _defaultColor,
        ),
        activeIcon: Icon(
          icon,
          color: _activeColor,
        ),

       title: Text(title,style:TextStyle(
         color:_currentIndex!=index?_defaultColor:_activeColor
       ) ,)
      );

   }

}