import 'package:flutter/material.dart';

/**
 *
 *  创建人:xuqing
 *  创建时间：2020年2月12日23:10:48
 *  类说明：卡片基类
 *
 *
 *
 */

class BaseCared extends StatefulWidget {
  BaseCared({Key key}) : super(key: key);
  @override
  BaseCaredState createState() {
    return BaseCaredState();
  }
}
class BaseCaredState extends State<BaseCared> {
  Color subTitleColor=Colors.grey;
  Color bottomTitleColor=Colors.grey;
  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return PhysicalModel(color: Colors.transparent,
      borderRadius: BorderRadius.circular(6),
      clipBehavior: Clip.antiAlias,
      child: Container(
        decoration: BoxDecoration(color: Colors.white),
        child: Column(
          children: <Widget>[
            topContent(),
            bottomContent()
          ],
        ),
      ),
     );
  }
  topContent(){
    return Padding(
      padding: EdgeInsets.only(left: 20,top: 26,bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              topTitle(""),
              topTtile2(),
            ],
          ),
          subTitle(""),
        ],
      ),
    );
  }
  bottomContent(){
  return Container();
  }

 Widget subTitle(String str){
    return Padding(
      padding: EdgeInsets.only(top: 5),
      child: Text(
          str,style: TextStyle(
          fontSize: 11,
        color: subTitleColor
      ),
      ),
    );
  }

  topTitle(String title){
  return Text(title,style: TextStyle(
    fontSize: 22
      ),);
  }
  topTtile2( ){
   return Container();
  }

  bottomTitle(String title){
    return Text(title,style: TextStyle(
      fontSize: 11,
      color: bottomTitleColor
    ),
    textAlign: TextAlign.center,
    );
  }
}