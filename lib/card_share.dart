
import 'base_cared.dart';
import 'base_cared.dart';
import 'package:flutter/material.dart';

/**
 *  创建人：xuqing
 *  创建时间：2020年2月15日00:53:06
 *  类说明：分享页面
 *
 */


class CardShare extends BaseCared{

    @override
    _CardShareState createState() {
    // TODO: implement createState
    return _CardShareState();
  }

}

class  _CardShareState extends BaseCaredState{
  String  imageUrl="http://www.devio.org/io/flutter_beauty/card_list.png";


  @override
  void initState() {
    // TODO: implement initState
    subTitleColor=Color(0xffb99444);
    super.initState();
  }


    @override
  topTitle(String title) {
    // TODO: implement topTitle
    return super.topTitle("分享的联名卡");
  }

  @override
  Widget subTitle(String str) {
    // TODO: implement subTitle
    return super.subTitle("分享给朋友圈最多可获得12天无限卡");
  }


  @override
  topTtile2() {
    // TODO: implement topTtile2
    return Padding(
      padding: EdgeInsets.only(bottom: 5),
      child: Text("/19期限",style: TextStyle(
        fontSize: 11,
        color: Colors.grey
      ),),
    );
  }

  @override
  bottomContent() {
    // TODO: implement bottomContent
    return Expanded(
      child: Container(
      decoration: BoxDecoration(color: Color(0xfff6f7f9)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(top: 20,left: 15,bottom: 20),
                child: Image.network(imageUrl),
              ),
            ),

            Container(
              alignment: AlignmentDirectional.center,
               child: Padding(padding: EdgeInsets.only(bottom: 20),
                  child:   bottomTitle("29876678人 ，参与活动"),
                )
            )

          ],
        ),
      ),
    );
  }
}