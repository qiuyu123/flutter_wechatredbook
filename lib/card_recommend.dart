
import 'base_cared.dart';
import 'package:flutter/material.dart';

/**
 *
 *  创建人:xuqing
 *  创建时间：2020年2月12日23:14:55
 *  类说明:本周推荐
 *
 *
 *
 */

class CardRecommend extends BaseCared {
    @override
    _CardRecommendState createState() {
    // TODO: implement createState
    return _CardRecommendState();
  }

}

class  _CardRecommendState extends BaseCaredState{
  String  imageUrl="http://www.devio.org/io/flutter_beauty/card_1.jpg";
   @override
  void initState() {
    // TODO: implement initState
     subTitleColor=Color(0xffb99444);
    super.initState();
  }

  @override
  bottomContent() {
    // TODO: implement bottomContent
    return Expanded(
      //高度撑满
      child: Container(
        constraints: BoxConstraints.expand(),
        margin: EdgeInsets.only(top: 10),
       child: Image.network(imageUrl,
       fit: BoxFit.cover, //宽高充满容器  会裁切
       ),
      ),
    );
  }
 @override
  topTitle(String title) {
    // TODO: implement topTitle
    return super.topTitle("本周推荐");
  }

   @override
  Widget subTitle(String str) {
    // TODO: implement subTitle
    return super.subTitle("送你一张无限卡.全场数据免费读");
  }

}