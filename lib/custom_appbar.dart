import 'package:flutter/material.dart';


/***
 *  创建人 ：xuqing
 *  创建时间：2020年2月9日15:53:54
 *  类说明：自定义appbar
 *
 *
 */

class CustomAppbar extends StatefulWidget {
  CustomAppbar({Key key}) : super(key: key);

  @override
  _CustomAppbarState createState() {
    return _CustomAppbarState();
  }
}

class _CustomAppbarState extends State<CustomAppbar> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double paddingtop=MediaQuery.of(context).padding.top;
    return Container(
      margin: EdgeInsets.fromLTRB(20, paddingtop+10, 20, 5),
      padding: EdgeInsets.fromLTRB(20, 30, 20, 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(19),color: Colors.white60
      ),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.search,
            color: Colors.grey,
          ),
          Expanded(
           child: Text("长安十二实诚",style:
             TextStyle(
               fontSize: 15,
               color: Colors.grey
             ),),
          ),
          Container(
            width: 1,
            height: 20,
            margin: EdgeInsets.only(right: 13),
            decoration: BoxDecoration(
              color: Colors.grey
            ),
          ),
          Text(
            "书城",
            style: TextStyle(
              fontSize: 13,
            ),
          )
        ],
      ),
    );

  }


}