import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'custom_appbar.dart';
import 'card_recommend.dart';
import 'card_share.dart';
import 'care_free.dart';
import 'card_special.dart';






class ContentPager extends StatefulWidget {
  final ValueChanged<int>onPageChanged;
  final ContentPagerConteroller contentPagerConteroller;
   //构造方法 可选参数
  ContentPager({Key key, this.onPageChanged, this.contentPagerConteroller}) : super(key: key);
  @override
  _ContentPagerState createState() {
    return _ContentPagerState();
  }
}
class _ContentPagerState extends State<ContentPager> {
    //视图比例
  PageController _pageController=PageController(
    viewportFraction: 0.8,
  );
  static List<Color>_colorlist=[
    Colors.blue,
    Colors.red,
    Colors.deepOrange,
    Colors.teal,
  ];
  @override
  void initState() {
    // TODO: implement initState
    if(widget.contentPagerConteroller!=null){
      widget.contentPagerConteroller._pageController=_pageController;
    }
    _statusBar();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
       CustomAppbar(),
        Expanded(
          child: PageView(
            onPageChanged: widget.onPageChanged,
            controller: _pageController,
            children: <Widget>[
              _wrapItem(CardRecommend()),
              _wrapItem(CardShare()),
              _wrapItem(CardFree()),
              _wrapItem(CardSpecial()),
            ],
          ),
        )

      ],
    );
  }

  Widget   _wrapItem(Widget widget){
    return  Padding(padding: EdgeInsets.all(10.0),
       child: widget
       );

  }

   _statusBar(){
     SystemUiOverlayStyle  uiOverlayStyle=SystemUiOverlayStyle(
       systemNavigationBarColor: Color(0xFF000000),
       systemNavigationBarDividerColor: null,
       statusBarColor: Colors.transparent,
       systemNavigationBarIconBrightness: Brightness.light,
       statusBarIconBrightness: Brightness.dark,
       statusBarBrightness: Brightness.light,
     );
     SystemChrome.setSystemUIOverlayStyle(uiOverlayStyle);
   }
}

class  ContentPagerConteroller{
  PageController _pageController;
  void  jumpTopage(int page){
    //dart 编程技巧 安全调用
    _pageController?.jumpToPage(page);
  }

}